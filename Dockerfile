# File: avc-docker-jenkins-slave-ssh/Dockerfile
#
# Use to build the image: avcompris/jenkins-slave-ssh

FROM debian:9.11
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

RUN apt-get install -y \
	apt-utils \
	curl \
	git \
	gnupg2 \
	openjdk-8-jdk \
	ssh

#-------------------------------------------------------------------------------
#   2. SSHD
#-------------------------------------------------------------------------------

RUN mkdir -p /var/run/sshd

#-------------------------------------------------------------------------------
#   3. USERS
#-------------------------------------------------------------------------------

RUN useradd -m -s /bin/bash jenkins

USER jenkins
WORKDIR /home/jenkins

RUN mkdir .ssh/
RUN chmod 700 .ssh/
RUN touch .ssh/authorized_keys
RUN chmod 600 .ssh/authorized_keys

USER root
WORKDIR /

#-------------------------------------------------------------------------------
#   4. JENKINS AGENT
#-------------------------------------------------------------------------------

USER jenkins
WORKDIR /home/jenkins

COPY agent/2.217/*.jar ./

USER root
WORKDIR /

#-------------------------------------------------------------------------------
#   7. SCRIPTS
#-------------------------------------------------------------------------------

COPY entrypoint.sh .

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 22

CMD [ "/bin/bash", "entrypoint.sh" ]



