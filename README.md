# avc-docker-jenkins-slave-ssh

Docker image: avcompris/jenkins-slave-ssh

Usage:

	$ docker run \
		-e JENKINS_PUB_KEY="ssh-rsa AAAAB3Nz...NLsug/a7" \
		avcompris/jenkins-slave-ssh

Exposed port is 22.
